<?php
	$end = '2015-10-25'; // ngày kết thúc dự án
	$inputtime = strtotime($end) - time();
	$settime = date('d:H:i:s',$inputtime);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>CÔNG TY CỔ PHẦN LILAMA 18.1</title>
	<meta name="description" content="Công ty thiết kế website hàng đầu Việt Nam">
	<meta name="keywords" content="thiet ke website, website, thiet ke, do hoa, thiet ke thuong hieu">
	<link rel="icon" type="image/x-icon" href="images/favicon.ico">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js" type="text/javascript" charset="utf-8"></script>    	
	<link href="css/style.css" rel='stylesheet' type='text/css' />
    <script type="text/javascript">
      $(function(){
        $('#counter').countdown({
          image: 'images/digits.png',
          startTime: '<?= $settime?>'
        });      
      });
    </script>    
</head>
<body>
<div class='wapper'>
	<div class='outer'>
		<div class='logo'><img src='images/logo.png' /></div>
		<h1 class="title">
			CÔNG TY CỔ PHẦN LILAMA 18.1 - LILAMA 18.1 JSC
		</h1>
		<p style="color:#000;text-align:center">
            OFFICE : 9th floor Empire Tower, 26 - 28 HAM NGHI Str. – BEN NGHE Ward – Dist. 1 – HCM  City <Br />
			Tel : 84-8-62843555 - Fax : 84-8-62843777<Br />
			E-mail: info@lilama18-1.com.vn. - Website: www.lilama18-1.com.vn<Br />
		</p>
		<div class="counter">
		  <div id="counter"></div>
		  <div class="desc">
			<ul>
				<li>Ngày</li>
				<li>Giờ</li>
				<li>Phút</li>
				<li>Giây</li>
			</ul>		
			<div class='clr'></div>
		  </div>
		</div>

		<!--begin set up-->
		<div class='setup'>
			<div class="content">
				Website đang trong quá trình nâng cấp phiên bản. Xin lỗi vì sự bất tiện này.<br>
			  (DURING THE WEBSITE IS UPGRADED VERSION. Sorry for the inconvenience.)<br>
			  <img src="images/gears.gif">
			  <div class="clr"></div>
			</div>
			<div class='clr'></div>
			
		</div>
	</div>
</div>
<!--end set up-->
</body></html>




<script>
	/*
 * jquery-counter plugin
 *
 * Copyright (c) 2009 Martin Conte Mac Donell <Reflejo@gmail.com>
 * Dual licensed under the MIT and GPL licenses.
 * http://docs.jquery.com/License
 */
jQuery.fn.countdown = function(userOptions)
{
  // Default options
  var options = {
    stepTime: 60,
    // startTime and format MUST follow the same format.
    // also you cannot specify a format unordered (e.g. hh:ss:mm is wrong)
    format: "dd:hh:mm:ss",
    startTime: "01:00:00:00",
    digitImages: 6,
    digitWidth: 53,
    digitHeight: 77,
    timerEnd: function(){},
    image: "digits.png"
  };
  var digits = [], interval;

  // Draw digits in given container
  var createDigits = function(where) 
  {
    var c = 0;
    // Iterate each startTime digit, if it is not a digit
    // we'll asume that it's a separator
    for (var i = 0; i < options.startTime.length; i++)
    {
      if (parseInt(options.startTime[i]) >= 0) 
      {
        elem = $('<div id="cnt_' + i + '" class="cntDigit" />').css({
          height: options.digitHeight * options.digitImages * 10, 
          float: 'left', background: 'url(\'' + options.image + '\')',
          width: options.digitWidth});
        digits.push(elem);
        margin(c, -((parseInt(options.startTime[i]) * options.digitHeight *
                              options.digitImages)));
        digits[c].__max = 9;
        // Add max digits, for example, first digit of minutes (mm) has 
        // a max of 5. Conditional max is used when the left digit has reach
        // the max. For example second "hours" digit has a conditional max of 4 
        switch (options.format[i]) {
          case 'h':
            digits[c].__max = (c % 2 == 0) ? 2: 9;
            if (c % 2 == 0)
              digits[c].__condmax = 4;
            break;
          case 'd': 
            digits[c].__max = 9;
            break;
          case 'm':
          case 's':
            digits[c].__max = (c % 2 == 0) ? 5: 9;
        }
        ++c;
      }
      else 
        elem = $('<div class="cntSeparator"/>').css({float: 'left'})
                .text(options.startTime[i]);

      where.append(elem)
    }
  };
  
  // Set or get element margin
  var margin = function(elem, val) 
  {
    if (val !== undefined)
      return digits[elem].css({'marginTop': val + 'px'});

    return parseInt(digits[elem].css('marginTop').replace('px', ''));
  };

  // Makes the movement. This is done by "digitImages" steps.
  var moveStep = function(elem) 
  {
    digits[elem]._digitInitial = -(digits[elem].__max * options.digitHeight * options.digitImages);
    return function _move() {
      mtop = margin(elem) + options.digitHeight;
      if (mtop == options.digitHeight) {
        margin(elem, digits[elem]._digitInitial);
        if (elem > 0) moveStep(elem - 1)();
        else 
        {
          clearInterval(interval);
          for (var i=0; i < digits.length; i++) margin(i, 0);
          options.timerEnd();
          return;
        }
        if ((elem > 0) && (digits[elem].__condmax !== undefined) && 
            (digits[elem - 1]._digitInitial == margin(elem - 1)))
          margin(elem, -(digits[elem].__condmax * options.digitHeight * options.digitImages));
        return;
      }

      margin(elem, mtop);
      if (margin(elem) / options.digitHeight % options.digitImages != 0)
        setTimeout(_move, options.stepTime);

      if (mtop == 0) digits[elem].__ismax = true;
    }
  };

  $.extend(options, userOptions);
  this.css({height: options.digitHeight, overflow: 'hidden'});
  createDigits(this);
  interval = setInterval(moveStep(digits.length - 1), 1000);
};

</script>